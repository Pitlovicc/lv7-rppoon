﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7.zadatak
{
    class Cart : IItem
    {
        private int count;
        private List<IItem> items;
        public Cart(List<IItem> items) { this.items = items; }
        public Cart() { this.items = new List<IItem>(); }
        public void AddItem(IItem item) { this.items.Add(item); }
        public void RemoveItem(IItem item) { this.items.Remove(item); }

        public double Accept(IVisitor visitor) {
            
            foreach (IItem item in items)
            {
                item.Accept(visitor);
                count++;
            }
        return count;    
        }
    }
}
