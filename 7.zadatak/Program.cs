﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7.zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            Book book = new Book("Dundo Maroje", 110);
            VHS vhs = new VHS("Rođendan snimka", 120);
            DVD dvd = new DVD("Umri muški", DVDType.SOFTWARE, 120);

            RentVisitor rentVisitor = new RentVisitor();
            Cart cart = new Cart();
            cart.AddItem(book);
            cart.AddItem(vhs);
            cart.AddItem(dvd);

            Console.WriteLine(cart.Accept(rentVisitor));
            
        }
    }
}
