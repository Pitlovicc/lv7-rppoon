﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4.zadatak
{
    class SystemDataProvider : SimpleSystemDataProvider
    {
        private double previousCPULoad;
        private double previousRAMAvailable;
        public SystemDataProvider() : base()
        {
            this.previousCPULoad = this.CPULoad;
            this.previousRAMAvailable = this.AvailableRAM;
        }
        public double GetCPULoad()
        {
            double currentLoad = this.CPULoad;
            if (currentLoad > (previousCPULoad += 0.1 * previousCPULoad) || currentLoad < (previousRAMAvailable += 0.1 * previousCPULoad))
            {
                this.Notify();
            }
            this.previousCPULoad = currentLoad;
            return currentLoad;
        }
        public double GetAvailableRAM()
        {
            double currentLoad =this.AvailableRAM;
            if (currentLoad != this.previousRAMAvailable)
                if (currentLoad > (previousRAMAvailable += 0.1 * previousRAMAvailable) || currentLoad <(previousRAMAvailable += 0.1 * previousRAMAvailable))
            {
                this.Notify();
            }
            this.previousRAMAvailable = currentLoad;
            return currentLoad;
        }
    }
}
