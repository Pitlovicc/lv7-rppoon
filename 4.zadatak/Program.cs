﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4.zadatak
{
    class Program
    {
        static void Main(string[] args)
        {

            SystemDataProvider systemDataProvider = new SystemDataProvider();
            ConsoleLogger consoleLogger = new ConsoleLogger();
            systemDataProvider.Attach(consoleLogger);
            consoleLogger.Log(systemDataProvider);


            for (; ; )
            {
                systemDataProvider.GetCPULoad();
                systemDataProvider.GetAvailableRAM();

                System.Threading.Thread.Sleep(1000);
            }
        }
    }
}
