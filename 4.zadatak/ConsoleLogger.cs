﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4.zadatak
{
    class ConsoleLogger : Logger
    {
        public void Log(SimpleSystemDataProvider provider)
        {
            Console.WriteLine("Procesor:" + provider.CPULoad);
            Console.WriteLine("Ram " + provider.AvailableRAM);
        }
    }
}
