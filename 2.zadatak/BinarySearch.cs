﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2.zadatak
{
    class BinarySearch : SearchStrategy
    {
        public override double binarySearch(double[] arr, int number)
        {
            int l = 0, r = arr.Length - 1;
            while (l <= r)
            {
                int m = l + (r - l) / 2;

                if (arr[m] == number)
                {
                    Console.WriteLine("Broj je pronađen " + arr[m]);
                    return m;
                }
                if (arr[m] < number)
                    l = m + 1;
                else
                    r = m - 1;
            }
            Console.WriteLine("Broj nije pronađen");
            return -1;
        }
    }
}
