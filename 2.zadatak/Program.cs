﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2.zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            NumberSequence numberSequence = new NumberSequence(10);
            Random randomGenerator = new Random();
            for (int i = 1; i < 10; i++)

            {
                numberSequence.InsertAt(i, randomGenerator.Next(1, 15));
            }
            Console.WriteLine("Polje:" + "\n" + numberSequence.ToString());
            SearchStrategy search = new BinarySearch();
            numberSequence.SetSearchStrategy(search);
            numberSequence.Search();        }
    }
}
