﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2.zadatak
{
    abstract class SearchStrategy
    {
        public abstract double binarySearch(double[] array,  int number);
    }
}
