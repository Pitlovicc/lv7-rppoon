﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6.zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            Book book = new Book("Dundo Maroje", 110);
            VHS vhs = new VHS("Rođendan snimka", 120);
            DVD dvd = new DVD("Umri muški", DVDType.SOFTWARE, 120);

            RentVisitor rentVisitor = new RentVisitor();
            Console.WriteLine("Price of renting this book: " + book.Accept(rentVisitor) + "kn" ) ;
            Console.WriteLine("Price of renting this VHS: " + vhs.Accept(rentVisitor) + "kn");
            Console.WriteLine("Price of renting this DVD: " + dvd.Accept(rentVisitor) + "kn");
        }
    }
}
