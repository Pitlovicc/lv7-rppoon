﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _6.zadatak
{
    class RentVisitor : IVisitor
    {
        
        public double Visit(DVD DVDItem)
        {
            if (DVDItem.Type == DVDType.SOFTWARE)
                return double.NaN;
            else
                return 0.1 * DVDItem.Price; 
        }
        public double Visit(VHS VHSItem)
        {
            return  0.1 * VHSItem.Price;
        }

        public double Visit(Book BookItem)
        {
            return  0.1 * BookItem.Price;
        }
    }
}
