﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _5.zadatak
{
    class Program
    {
        static void Main(string[] args)
        {
            Book book = new Book("Dundo Maroje", 110);
            VHS vhs = new VHS("Rođendan snimka", 120);
            DVD dvd = new DVD("Umri muški",DVDType.MOVIE, 120);

            BuyVisitor buyVisitor = new BuyVisitor();
            Console.WriteLine("Book:" + book.Title + " " + book.Accept(buyVisitor)+"kn");
            Console.WriteLine("VHS:" + vhs.Title + " " + vhs.Accept(buyVisitor)+"kn");
            Console.WriteLine("DVD:" + dvd.Description + " " + dvd.Accept(buyVisitor)+"kn");

            
        }
    }
}
